<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Diary_taken extends Model
{
    protected $fillable = [
        'buyer_id', 'amount'
    ];
}
