<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BuyerController extends Controller
{
    public function eloquentlist(){
        $buyers = \App\buyers::all();
        $lists = [];
            foreach ($buyers as $key => $buyer) {
                foreach ($buyer->diary_taken  as $k => $value) {
                    $diary = $value->total_diary;
                    $eraser = $buyer->eraser_taken[$k]->total_eraser;
                    $pen = $buyer->pen_taken[$k]->total_pen;
                  
                   $total = $eraser+$pen+$diary;
                }
            
                array_push($lists,['id' => $buyer->id,'name' => $buyer->name,'diary' => $diary,'pen' => $pen, 'eraser' => $eraser,'total' => $total]);
            }
            $ra = array_reverse($lists);
        return view('purchase_list')->with('lists', $ra);
    }
    public function list(){

        $diary_results = DB::select( DB::raw("SELECT b.id,b.name,
        sum(case when d.buyer_id then d.amount end) AS total_diary
        FROM buyers AS b
        LEFT JOIN diary_takens AS d ON b.id = d.buyer_id
        GROUP BY b.id") );
        $pen_results = DB::select( DB::raw("SELECT b.id,b.name,
        sum(case when p.buyer_id then p.amount end) AS total_pen
        FROM buyers AS b
        LEFT JOIN pen_takens AS p ON b.id = p.buyer_id
        GROUP BY b.id") );
        $eraser_results = DB::select( DB::raw("SELECT b.id,b.name,
        sum(case when e.buyer_id then e.amount end) AS total_eraser
        FROM buyers AS b
        LEFT JOIN eraser_takens AS e ON b.id = e.buyer_id
        GROUP BY b.id") );
        $lists = [];
            foreach ($diary_results as $key => $value) {
                //dd($value);
                $total = ($value->total_diary) + ($eraser_results[$key]->total_eraser) + ($pen_results[$key]->total_pen);
                array_push($lists,['id' => $value->id,'name' => $value->name,'diary' => $value->total_diary,'pen' => $pen_results[$key]->total_pen, 'eraser' => $eraser_results[$key]->total_eraser,'total' => $total]);
            }
            $ra = array_reverse($lists);
          //  dd($ra);
        return view('purchase_list_no')->with('lists', $ra);
    }
    public function second(){
        $diary_results = DB::select( DB::raw("SELECT b.id,b.name,
        sum(case when d.buyer_id then d.amount end) AS total_diary
        FROM buyers AS b
        LEFT JOIN diary_takens AS d ON b.id = d.buyer_id
        GROUP BY b.id") );
        $pen_results = DB::select( DB::raw("SELECT b.id,b.name,
        sum(case when p.buyer_id then p.amount end) AS total_pen
        FROM buyers AS b
        LEFT JOIN pen_takens AS p ON b.id = p.buyer_id
        GROUP BY b.id") );
        $eraser_results = DB::select( DB::raw("SELECT b.id,b.name,
        sum(case when e.buyer_id then e.amount end) AS total_eraser
        FROM buyers AS b
        LEFT JOIN eraser_takens AS e ON b.id = e.buyer_id
        GROUP BY b.id") );
        $lists = [];
            foreach ($diary_results as $key => $value) {
                $total = ($value->total_diary) + ($eraser_results[$key]->total_eraser) + ($pen_results[$key]->total_pen);
                if ($key == '1') {
                    array_push($lists, ['id' => $value->id,'name' => $value->name,'diary' => $value->total_diary,'pen' => $pen_results[$key]->total_pen, 'eraser' => $eraser_results[$key]->total_eraser,'total' => $total]);
                }
            }
        return view('second_no')->with('lists', $lists);
    }
    public function eloquentsecond(){
        $buyers = \App\buyers::all();
        $lists = [];
            foreach ($buyers as $key => $buyer) {
                foreach ($buyer->diary_taken  as $k => $value) {
                    $diary = $value->total_diary;
                    $eraser = $buyer->eraser_taken[$k]->total_eraser;
                    $pen = $buyer->pen_taken[$k]->total_pen;
                    $total = $eraser+$pen+$diary;
                }
                if($key == '1'){
                    array_push($lists,['id' => $buyer->id,'name' => $buyer->name,'diary' => $diary,'pen' => $pen, 'eraser' => $eraser,'total' => $total]);
                } 
            }
        return view('purchase_list')->with('lists', $lists);
    }
    public function dbinsert(){
        $json = file_get_contents(storage_path('app/public/records.json'));
        $objs = json_decode($json,true);
        foreach ($objs as $obj)  {
            foreach ($obj as $key => $values) {
                $insertArr = [
                    'id' => $values['id'],
                    'from_statement' => $values['from_statement'],
                    'financial_instrument_code' => $values['financial_instrument_code'],
                    'action' => $values['action'],
                    'entry_price' => $values['entry_price'],
                    'closed_price' => $values['closed_price'],
                    'take_profit_1' => $values['take_profit_1'],
                    'stop_loss_1' => $values['stop_loss_1'],
                    'signal_result' => $values['signal_result'],
                    'statement_batch' => $values['statement_batch'],
                    'closed_on' => $values['closed_on'],
                ];
                DB::table('records')->insert($insertArr);
            }
            dd("Finished adding data in records table");
        }
    }
}
