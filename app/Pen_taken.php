<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pen_taken extends Model
{
    protected $fillable = [
        'buyer_id', 'amount'
    ];
}
