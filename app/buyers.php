<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class buyers extends Model
{
    protected $fillable = [
        'name', 'status',
    ];
    public function diary_taken(){
        return $this->hasMany(Diary_taken::class,'buyer_id', 'id')->selectRaw('SUM(amount) as total_diary')
        ->groupBy('buyer_id');
    }
    public function pen_taken(){
        return $this->hasMany(Pen_taken::class,'buyer_id', 'id')->selectRaw('SUM(amount) as total_pen')
        ->groupBy('buyer_id');
    }
    public function eraser_taken(){
        return $this->hasMany(Pen_taken::class,'buyer_id', 'id')->selectRaw('SUM(amount) as total_eraser')
        ->groupBy('buyer_id');
    }
}
