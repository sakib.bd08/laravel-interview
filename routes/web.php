<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('purchase-list-eloquent', 'BuyerController@eloquentlist');
Route::get('purchase-list-no-eloquent', 'BuyerController@list');
Route::get('second-buyer-eloquent', 'BuyerController@eloquentsecond');
Route::get('second-buyer-no-eloquent', 'BuyerController@second');
Route::get('sort-js', function () {
    return '<pre>var items = [
        { name: "Sharpe", value: 37 },
        { name: "Edward", value: 21 },
        { name: "And", value: 45 },
        { name: "The", value: -12 },
];

items.sort(function (a, b) {
  return a.value - b.value;
});

items.sort(function(a, b) {
  var nameA = a.name.toUpperCase(); 
  var nameB = b.name.toUpperCase(); 
  if (nameA < nameB) {
    return -1;
  }
  if (nameA > nameB) {
    return 1;
  }
  return 0;
});</pre>';
});
Route::get('foreach-js', function () {
    return '<pre>var sum = 0;
    var numbers = [65, 44, 12, 4];
    numbers.forEach(myFunction);
    
    function myFunction(item) {
      sum += item;
      document.getElementById("demo").innerHTML = sum;
    };</pre>';
});

Route::get('filter-js', function () {
    return '<pre>
    let arr = [
        { id: 15 },
        { id: -1 },
        { id: 0 },
        { id: 3 },
        { id: 12.2 },
        { id: null }
      ]
      
      let invalidEntries = 0
      
      function filterByID(item) {
        if (Number.isFinite(item.id) && item.id !== 0) {
          return true
        } 
        invalidEntries++
        return false;
      }
      let arrByID = arr.filter(filterByID)
    </pre>';
});
Route::get('map-js', function () {
    return '<pre>
    let kvArray = [{key: 1, value: 10}, 
               {key: 2, value: 20}, 
               {key: 3, value: 30}]

let reformattedArray = kvArray.map(obj => {
   let rObj = {}
   rObj[obj.key] = obj.value
   return rObj
})
// reformattedArray is now [{1: 10}, {2: 20}, {3: 30}], 
    </pre>';
});


Route::get('reduce-js', function () {
    return '<pre>
    var total = [0, 1, 2, 3, 4].reduce(function(a, b) {
        console.log("a: " + a);
        console.log("b: " + b);
        return a + b;
      }, -10); 
    </pre>';
});
Route::get('/record-transfer', 'BuyerController@dbinsert');
Route::get('define-callback-js', function () {
    return '<pre>
     data = {email:"trump@gmail.com", age:70}; 
            checkAge(data, function(email){
                if(data.age < 18){
                    console.log("Email is not valid"). 
                }
            });
    </pre>';
});

Route::get('animation',function () {
    return view('animation');
});
Route::get('i-m-funny',function () {
    return view('i_m_funny');
});

